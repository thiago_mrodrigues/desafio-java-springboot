package br.com.compasso.configuration.exception;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by Thiago Rodrigues on 08/07/2021
 */
@ControllerAdvice
public class ProductExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String status_code = "400";

        String message = "Parametros invalidos";

        return handleExceptionInternal(ex, new Erro(status_code, message), headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        String status_code = "404";
        String message = "Produto não encontrado";

        return handleExceptionInternal(ex, new Erro(status_code, message), new HttpHeaders(), HttpStatus.NOT_FOUND, request);

    }

    @ExceptionHandler({PriceNegativeException.class})
    public ResponseEntity<Object> handlerNegativePriceExceptin(PriceNegativeException ex, WebRequest request){
        String status_code = "400";
        String message = "Price precisa ser positivo";
        return handleExceptionInternal(ex, new Erro(status_code, message), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    public static class Erro {
        private String status_code;
        private String message;

        public Erro(String status_code, String message) {
            this.status_code = status_code;
            this.message = message;
        }

        public String getStatus_code() {
            return status_code;
        }

        public String getMessage() {
            return message;
        }
    }

}
