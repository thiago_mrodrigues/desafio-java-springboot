package br.com.compasso.repository;

import br.com.compasso.model.Product;
import org.springframework.stereotype.Repository;

/**
 * Created by Thiago Rodrigues on 07/07/2021
 */
@Repository
public interface ProductRepository extends BaseRepository<Product, Long> {

}
