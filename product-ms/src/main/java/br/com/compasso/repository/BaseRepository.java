package br.com.compasso.repository;

import br.com.compasso.model.BaseModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Created by Thiago Rodrigues on 08/07/2021
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseModel, ID extends Serializable> extends JpaRepository<T, ID> {
}
