package br.com.compasso.service;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thiago Rodrigues on 07/07/2021
 */

public interface IBaseService<T> {
    Optional<T> findById(long id);
    List<T> findAll();
    Page<T> findAllPaginated(int page, int size);
    T save(T t);
    void remove(T t);
    Long count();
}
