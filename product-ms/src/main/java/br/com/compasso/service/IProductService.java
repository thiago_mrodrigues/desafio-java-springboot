package br.com.compasso.service;

import br.com.compasso.model.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Thiago Rodrigues on 08/07/2021
 */
public interface IProductService extends IBaseService<Product> {
    List<Product> searchWithParams (BigDecimal minPrice, BigDecimal maxPrice, String q);

}
