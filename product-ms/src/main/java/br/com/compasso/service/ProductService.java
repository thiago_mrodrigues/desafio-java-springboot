package br.com.compasso.service;

import br.com.compasso.configuration.exception.PriceNegativeException;
import br.com.compasso.model.Product;
import br.com.compasso.repository.BaseRepository;
import br.com.compasso.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thiago Rodrigues on 08/07/2021
 */
@Service
public class ProductService extends BaseService<Product> implements IProductService {

    @Autowired
    EntityManager em;

    @Autowired
    private ProductRepository repo;

    @Override
    protected BaseRepository<Product, Long> getRepository() {
        return repo;
    }

    @Override
    public Product save(Product product) {

        if (product.getPrice().compareTo(BigDecimal.ZERO) < 0){
            throw new PriceNegativeException();
        }
        return super.save(product);
    }

    // Busca os produtos com base nos filtros passados
    @Override
    public List<Product> searchWithParams(BigDecimal minPrice, BigDecimal maxPrice, String q) {
        this.validaParametros(minPrice, maxPrice);

        StringBuilder sqlQuery = new StringBuilder();
        List<Product> productsFound = new ArrayList<>();
        String parametroBusca = q;


        sqlQuery.append("SELECT * ");
        sqlQuery.append("FROM product p ");

        if (minPrice != null && maxPrice != null){
            sqlQuery.append("where p.price >= :_minPrice && p.price <= :_maxPrice ");

        } else if (minPrice != null){
            sqlQuery.append("where p.price >= :_minPrice ");

        } else if ( maxPrice != null){
            sqlQuery.append("where p.price <= :_maxPrice ");
        }

        if (q != null) {
            if (minPrice != null || maxPrice != null) {
                sqlQuery.append(" AND ");
            }

            if (minPrice == null && maxPrice == null) {
                sqlQuery.append(" WHERE ");
            }

            sqlQuery.append(" p.name like '%").append(parametroBusca).append("%' ")
                    .append("or")
                    .append(" p.description like '%").append(parametroBusca).append("%' ");

        }

        Query query = em.createNativeQuery(sqlQuery.toString());

        if (minPrice != null) {
            query.setParameter("_minPrice", minPrice);
        }

        if (maxPrice != null) {
            query.setParameter("_maxPrice", maxPrice);
        }

        System.out.println(query.toString());

        try {
            List<Object[]> result = (List<Object[]>) query.getResultList();

            if (!result.isEmpty()) {
                for(Object[] obj : result) {

                    Product product = new Product();
                    Long id = new Long(obj[0].toString());

                    product.setId(id);
                    product.setName((String)obj[1]);
                    product.setDescription((String)obj[2]);
                    product.setPrice((BigDecimal)obj[3]);

                    productsFound.add(product);
                }
            }

        } catch (NoResultException nre){
            return null;
        }

        // limpa a query
        sqlQuery.delete(0,sqlQuery.length());



        return productsFound;
    }

    // Verifica se os parametros são positivos
    private void validaParametros(BigDecimal minPrice, BigDecimal maxPrice) {
        if (minPrice.compareTo(BigDecimal.ZERO) < 0 || maxPrice.compareTo(BigDecimal.ZERO) < 0){
            throw new PriceNegativeException();
        }
    }
}
