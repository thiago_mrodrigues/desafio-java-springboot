package br.com.compasso.service;

import br.com.compasso.model.BaseModel;
import br.com.compasso.repository.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by Thiago Rodrigues on 08/07/2021
 */
@Service
public abstract class BaseService<T extends BaseModel> implements IBaseService<T> {

    protected abstract BaseRepository<T, Long> getRepository();

    @Override
    @Transactional(readOnly = true)
    public Optional<T> findById(long id){
        return getRepository().findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> findAll(){
        return getRepository().findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<T> findAllPaginated(int page, int size) {
        return getRepository().findAll(PageRequest.of(page, size));
    }

    @Override
    @Transactional
    public T save(T t){
        return getRepository().save(t);
    }

    @Override
    @Transactional
    public void remove(T t) {
        getRepository().delete(t);
    }

    @Override
    public Long count() {
        return getRepository().count();
    }
}
