package br.com.compasso.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by Thiago Rodrigues on 07/07/2021
 */
@Entity
@Table(name = "product")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable=false)
    @NotNull
    private String name;

    @Column(name = "description", nullable=false, length=12)
    @NotNull
    private String description;

    @Column(name = "price", nullable = false)
    @NotNull
    private BigDecimal price;
}
