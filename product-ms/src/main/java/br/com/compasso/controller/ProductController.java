package br.com.compasso.controller;

import br.com.compasso.model.Product;
import br.com.compasso.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Thiago Rodrigues on 07/07/2021
 */

@Slf4j
@RestController
@RequestMapping("/products")
@Api(value = "Microserviço Products")
public class ProductController extends AbstractRestController<Product> {

    @Autowired
    private IProductService service;

    @Override
    protected IProductService getService() {
        return service;
    }

    @Override
    @GetMapping
    @ApiOperation(value = "Retorna uma lista de produtos")
    public ResponseEntity<List<Product>> listar() {
        return super.listar();
    }

    @Override
    @GetMapping("/{id}")
    @ApiOperation(value = "Retorna um produto único")
    public ResponseEntity<Product> visualizar(@PathVariable long id) {
        return super.visualizar(id);
    }

    @Override
    @PostMapping
    @ApiOperation(value = "Salva um produto")
    public ResponseEntity cadastrar(@Valid @RequestBody Product product) {

        return super.cadastrar(product);
    }

    @Override
    @PutMapping("/{id}")
    @ApiOperation(value = "Atualiza um produto")
    public ResponseEntity editar(@PathVariable Long id, @Valid @RequestBody Product product) {
        return super.editar(id, product);
    }

    @Override
    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deleta um produto")
    public ResponseEntity excluir(@PathVariable long id) {
        return super.excluir(id);
    }

    /***
     * retornar a lista atual de todos os produtos filtrados de acordo com query parameters passados na URL
     * @param minPrice deverá bater o valor ">=" contra o campo price
     * @param maxPrice deverá bater o valor "<=" contra o campo price
     * @param q deverá bater o valor contra os campos name e description
     */
    @GetMapping("/search")
    @ApiOperation(value = "Retona uma lista de produto com base nos filtros passados")
    public ResponseEntity<List<Product>> searchWithFilters(@RequestParam(value = "min_price", required = false) BigDecimal minPrice,
                                                           @RequestParam(value = "max_price", required = false) BigDecimal maxPrice,
                                                           @RequestParam(value= "q", required = false) String q) {

        return ResponseEntity.ok(service.searchWithParams(minPrice, maxPrice, q));
    }


}
