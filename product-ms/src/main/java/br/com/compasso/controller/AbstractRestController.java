package br.com.compasso.controller;

import br.com.compasso.model.BaseModel;
import br.com.compasso.service.IBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

/**
 * Created by Thiago Rodrigues on 07/07/2021
 */

@Slf4j
public abstract class AbstractRestController<T extends BaseModel> {

    protected abstract IBaseService<T> getService();
    private String entityName;

    public ResponseEntity<List<T>> listar(){
        return ResponseEntity.ok(getService().findAll());
    }

    public ResponseEntity<T> visualizar(long id){
        log.debug("REST request to get {} : {}",getEntityName(), id);
        Optional<T> t = getService().findById(id);

        if (!t.isPresent())
            throw new EmptyResultDataAccessException(1);

        return ResponseEntity.ok(t.get());
    }

    public ResponseEntity cadastrar(T t){
        log.debug("REST request to save "+getEntityName()+" : {}", t);

        getService().save(t);

        return ResponseEntity.status(HttpStatus.CREATED).body(t);
    }

    public ResponseEntity editar(Long id, T t){
        log.debug("REST request to update " + getEntityName() +" : {}", t);
        Optional<T> tFound = getService().findById(id);

        if (!tFound.isPresent())
            throw new EmptyResultDataAccessException(1);

        getService().save(t);


        return ResponseEntity.status(HttpStatus.OK).body(t);
    }

    public ResponseEntity excluir(long id){
        log.debug("REST request to delete {} : {}", getEntityName(), id);
        Optional<T> t = getService().findById(id);

        if (!t.isPresent())
            throw new EmptyResultDataAccessException(1);

        getService().remove(t.get());


        return ResponseEntity.status(HttpStatus.OK).build();
    }

    protected String getEntityName(){
        if (entityName == null)
            entityName = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getSimpleName();

        return entityName;
    }

}
